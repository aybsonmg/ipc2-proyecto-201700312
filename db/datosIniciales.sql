/* ROLES QUE TENDRA LA APLICACIÓN */
INSERT INTO rol VALUES (1,"administrador");
INSERT INTO rol VALUES (2,"tecnico");
INSERT INTO rol VALUES (3,"agente");

/* TIPOS DE EMPRESA QUE PODRAN INSCRIBIRSE */
INSERT INTO tipo VALUES (1,"restaurante");
INSERT INTO tipo VALUES (2,"hotel");
INSERT INTO tipo VALUES (3,"museo");

/* REGIONES QUE ESTARAN DISPONIBLES */
INSERT INTO region VALUES (1, "norte");
INSERT INTO region VALUES (2, "sur");
INSERT INTO region VALUES (3, "este");
INSERT INTO region VALUES (4, "oeste");

/* ESTADOS POR LOS CUALES DEBE DE PASAR UNA EMPRESA */
INSERT INTO estado VALUES (1,"pre-evaluacion");
INSERT INTO estado VALUES (2,"en evaluacion");
INSERT INTO estado VALUES (3,"inscrito");

