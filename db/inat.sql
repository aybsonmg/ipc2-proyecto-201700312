/*
    CREACIÓN DE LA BASE DE DATOS DEL INSTITUTO NACIONAL DE TURISMO (INAT)
*/
CREATE DATABASE inat;
USE inat;

/*
    CREACIÓN DE LA TABLA DE ROLES Y DE LA TABLA DE USUARIO,
    QUE MANEJARA A LOS USUARIOS Y LOS PERMISOS QUE TENDRAN.
*/
CREATE TABLE rol(
    codigo INT PRIMARY KEY NOT NULL,
    rol VARCHAR(100) NOT NULL
);

CREATE TABLE usuario(
    dpi INT(13) PRIMARY KEY NOT NULL,
    nombre VARCHAR(200) NOT NULL,
    correo VARCHAR(200) NOT NULL,
    telefono INT(8) NOT NULL,
    contrasenia VARCHAR(100) NOT NULL,
    rol INT NOT NULL,
    FOREIGN KEY (rol) REFERENCES rol(codigo)
);

/*
    TABLA DE REGION QUE CONTENDRA LAS REGIONES DEL PAIS
*/
CREATE TABLE region(
    codigo INT PRIMARY KEY NOT NULL,
    region VARCHAR(200)
);

/*
    TABLA DE ESTADO QUE CONTIENE LOS ESTADOS POR LOS CUALES PASA 
    LA EMPRESA PARA SU INSCRIPCIÓN
*/
CREATE TABLE estado(
    codigo INT PRIMARY KEY NOT NULL,
    estado VARCHAR(200) NOT NULL
);

/*
    TABLA DE TIPO QUE CONTIENE LOS TIPOS DE EMPRESAS QUE VAN A 
    PODER SER ALMACENADOS
*/
CREATE TABLE tipo(
    codigo INT PRIMARY KEY NOT NULL,
    tipo VARCHAR(200) NOT NULL
);

/*
    TABLA SITIO TURISTICO QUE CONTENDRA LOS SITIOS TURISTICOS
    Y UNA TABLA DE IMAGENES QUE CONTENDRA LAS IMAGENES DE 
    DICHO SITIO TURISTICO
*/
CREATE TABLE sitio_turistico(
    codigo INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(200) NOT NULL,
    descripcion VARCHAR(500) NOT NULL,
    region INT NOT NULL,
    FOREIGN KEY (region) REFERENCES region(codigo)
);

CREATE TABLE fotografias_sitio(
    fotografia LONGBLOB NOT NULL,
    sitio_turistico INT NOT NULL,
    descripcion VARCHAR(500) NOT NULL,
    FOREIGN KEY (sitio_turistico) REFERENCES sitio_turistico(codigo)
);

/*
    TABLA EMPRESA QUE ALMACENA LAS EMPRESAS QUE SE 
    MOSTRARAN EN EL SITIO 
*/
CREATE TABLE empresa(
    codigo INT PRIMARY KEY NOT NULL,
    nombre VARCHAR(200) NOT NULL,
    direccion VARCHAR(300) NOT NULL,
    telefono INT(8) NOT NULL,
    correo VARCHAR(200) NOT NULL,
    fechaInscripcion DATE NULL,
    region INT NOT NULL,
    estado INT NOT NULL,
    tipo INT NOT NULL,
    FOREIGN KEY (region) REFERENCES region(codigo),
    FOREIGN KEY (estado) REFERENCES estado(codigo),
    FOREIGN KEY (tipo) REFERENCES tipo(codigo)
);

CREATE TABLE fotografias_empresa(
    fotografia LONGBLOB NOT NULL,
    empresa INT NOT NULL,
    descripcion VARCHAR(500) NOT NULL,
    FOREIGN KEY (empresa) REFERENCES empresa(codigo)
);

/*
    BOLETA QUE SE GENERA AL VERIFICAR LOS DATOS DE LA 
    EMPRESA
*/
CREATE TABLE boleta(
    codigo INT PRIMARY KEY NOT NULL,
    empresa INT NOT NULL,
    estado VARCHAR(200) NULL,
    FOREIGN KEY (empresa) REFERENCES empresa(codigo)
);

/*
    TABLAS QUE GUARDAN LA INFORMACIÓN EN PARTICULAR
    DE LOS SERVICIOS QUE OFRECE UN HOTEL Y LOS MUESTRA EN UN CATALOGO
*/
CREATE TABLE servicio(
    codigo INT PRIMARY KEY NOT NULL,
    servicio VARCHAR(500)
);

CREATE TABLE catologo_servicios(
    servicio INT NOT NULL,
    empresa INT NOT NULL,
    descripcion VARCHAR(500) NOT NULL,
    FOREIGN KEY (servicio) REFERENCES servicio(codigo),
    FOREIGN KEY (empresa) REFERENCES empresa(codigo)
);

/*
    TABLAS QUE GUARDAN LA INFORMACIÓN EN PARTICULAR
    DE LOS MUSEOS
*/
CREATE TABLE detalle_museo(
    empresa INT PRIMARY KEY NOT NULL,
    horarioEntrada DATETIME NOT NULL,
    horarioSalida DATETIME NOT NULL,
    tarifa FLOAT,
    FOREIGN KEY (empresa) REFERENCES empresa(codigo)
);

/*
    TABLAS QUE GUARDAN LA INFORMACIÓN EN PARTICULAR
    DE LAS ESPECIALIDADES DE LOS RESTAURANTES
    Y SU MENU, Y EL DETALLE DE LOS HORARIOS DE ATENCIÓN 
    DEL RESTAURANTE.
*/
CREATE TABLE especialidad(
    codigo INT PRIMARY KEY NOT NULL,
    especialidad VARCHAR(200) NOT NULL
);

CREATE TABLE menu(
    empresa INT NOT NULL,
    especialidad INT NOT NULL,
    platillo VARCHAR(100) NOT NULL,
    descripcion VARCHAR(500) NOT NULL,
    FOREIGN KEY (empresa) REFERENCES empresa(codigo),
    FOREIGN KEY (especialidad) REFERENCES especialidad(codigo)
);

CREATE TABLE detalle_restaurante(
    empresa INT PRIMARY KEY NOT NULL,
    horarioApertura DATETIME NOT NULL,
    horarioCierre DATETIME NOT NULL,
    FOREIGN KEY (empresa) REFERENCES empresa(codigo)
);

/*
    TABLA QUE CONTENDRA LAS VISITAS DE REGISTRO Y LAS VISITAS TECNICAS
    PARA UN TECNICO
*/
CREATE TABLE visita_registro(
    codigo INT PRIMARY KEY NOT NULL,
    fechaVisita DATE NOT NULL,
    dpiTecnico INT(13) NOT NULL,
    empresa INT NOT NULL,
    resolucion VARCHAR(100) NOT NULL,
    comentario VARCHAR(500) NULL,
    FOREIGN KEY (dpiTecnico) REFERENCES usuario(dpi),
    FOREIGN KEY (empresa) REFERENCES empresa(codigo)
);

CREATE TABLE visita_tecnica(
    codigo INT PRIMARY KEY NOT NULL,
    nombreRecorrido VARCHAR(100) NULL,
    dpiTecnico INT(13) NOT NULL,
    empresa INT NOT NULL,
    fechaInicio DATE NOT NULL,
    fechaFinal DATE NOT NULL,
    comentario VARCHAR(500) NULL,
    FOREIGN KEY (dpiTecnico) REFERENCES usuario(dpi),
    FOREIGN KEY (empresa) REFERENCES empresa(codigo)
);