﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class adminReporteHotel : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();

			//Region 1
			Literal1.Text = servicio("piscina", "1");
			Literal2.Text = servicio("internet", "1");
			Literal3.Text = servicio("room service", "1");
			Literal4.Text = servicio("gimnasio", "1");
			Literal5.Text = servicio("agua caliente", "1");
			Literal6.Text = servicio("vip", "1");

			//Region 2
			Literal7.Text = servicio("piscina", "2");
			Literal8.Text = servicio("internet", "2");
			Literal9.Text = servicio("room service", "2");
			Literal10.Text = servicio("gimnasio", "2");
			Literal11.Text = servicio("agua caliente", "2");
			Literal12.Text = servicio("vip", "2");

			//Region 3
			Literal13.Text = servicio("piscina", "3");
			Literal14.Text = servicio("internet", "3");
			Literal15.Text = servicio("room service", "3");
			Literal16.Text = servicio("gimnasio", "3");
			Literal17.Text = servicio("agua caliente", "3");
			Literal18.Text = servicio("vip", "3");

			//Region 4
			Literal19.Text = servicio("piscina", "4");
			Literal20.Text = servicio("internet", "4");
			Literal21.Text = servicio("room service", "4");
			Literal22.Text = servicio("gimnasio", "4");
			Literal23.Text = servicio("agua caliente", "4");
			Literal24.Text = servicio("vip", "4");
		}

		private string servicio(String service, String region)
		{
			String cantidad = "";
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM servicio s, empresa e, region r WHERE s.empresa = e.codigo AND r.codigo = e.region AND s.servicio = '" + service + "' AND e.region =" + region + " ; ";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					cantidad = dr.GetString(0);
				}
			}
			con.Close();
			return cantidad;
		}
	}
}