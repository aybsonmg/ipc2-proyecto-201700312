﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class agenteListarSitiosTuristicos : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;

		MySqlConnection con2 = new MySqlConnection();
		MySqlCommand com2 = new MySqlCommand();
		MySqlDataReader dr2;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
			con2.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT s.codigo, s.nombre, s.descripcion, r.region FROM sitio_turistico s, region r WHERE r.codigo = s.region; ";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					//datos
					String codigo = dr.GetString(0);
					String nombre = dr.GetString(1);
					String descripcion = dr.GetString(2);
					String region = dr.GetString(3);
					
					//imagen
					byte[] image = null;


					//consulta para obtener la imagen
					con2.Open();
					com2.Connection = con2;
					com2.CommandText = "SELECT fotografia FROM fotografia_sitio WHERE sitio_turistico = " + codigo + ";";

					image = (byte[]) com2.ExecuteScalar();
					string strBase64 = Convert.ToBase64String(image);

					/*
					<div class="carousel-item">
						<img class="d-block w-100" src="images/work-3.jpg" alt="Third slide">
						<div class="carousel-caption">
							<h3>Los Angeles</h3>
							<p class="des">We had such a great time in LA!</p>
						</div>
					</div> 
					*/

					if (region.ToLower() == "norte") {
						carrouselNorte.Text += "" +
						"<div class=\"carousel-item \">" +
						"	<img class=\"d-block w-100\" src=\"data:Image/png;base64," + strBase64 + "\" alt=\"Third slide\">" +
						"	<div class=\"carousel-caption\">" +
						"		<h3>"+ codigo + ". " + nombre.ToUpper() + "</h3>" +
						"		<p class=\"des\">" + descripcion.ToUpper() + "</p>" +
						"	</div>" +
						"</div>";
					} else if (region.ToLower() == "sur") {
						CarrouselSur.Text += "" +
						"<div class=\"carousel-item\">" +
						"	<img class=\"d-block w-100\" src=\"data:Image/png;base64," + strBase64 + "\" alt=\"Third slide\">" +
						"	<div class=\"carousel-caption\">" +
						"		<h3>" + codigo + ". " + nombre.ToUpper() + "</h3>" +
						"		<p class=\"des\">" + descripcion.ToUpper() + "</p>" +
						"	</div>" +
						"</div>";
					} else if (region.ToLower() == "este") {
						CarrouselEste.Text += "" +
						"<div class=\"carousel-item\">" +
						"	<img class=\"d-block w-100\" src=\"data:Image/png;base64," + strBase64 + "\" alt=\"Third slide\">" +
						"	<div class=\"carousel-caption\">" +
						"		<h3>" + codigo + ". " + nombre.ToUpper() + "</h3>" +
						"		<p class=\"des\">" + descripcion.ToUpper() + "</p>" +
						"	</div>" +
						"</div>";
					} else if (region.ToLower() == "oeste") {
						CarrouselOeste.Text += "" +
						"<div class=\"carousel-item\">" +
						"	<img class=\"d-block w-100\" src=\"data:Image/png;base64," + strBase64 + "\" alt=\"Third slide\">" +
						"	<div class=\"carousel-caption\">" +
						"		<h3>" + codigo + ". " + nombre.ToUpper() + "</h3>" +
						"		<p class=\"des\">" + descripcion.ToUpper() + "</p>" +
						"	</div>" +
						"</div>";
					}
					
					con2.Close();
				}
			}
			con.Close();


		}
	}
}