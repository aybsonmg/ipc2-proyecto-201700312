﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminRegistro.aspx.cs" Inherits="appProyecto2.adminRegistro" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administrador</title>
<!--===============================================================================================-->	
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <style>
        body {
            background: url("./images/bg.jpg");
            background-size: cover;
            background-attachment:fixed;
        }

        .colorFondo {
            background-color: rgba(0,0,0,0.7);
        }

        .dd {
            padding:25px;
            background: linear-gradient(60deg,rgba(10,50,100,0.7),rgba(0,0,0,0.5));
            margin-top: 50px;
            margin-bottom: 100px;
            color: #fff;
        }

   

    </style>
</head>
<body>
  
    <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
            <a class="navbar-brand" href="#">
                <h5>Instituto N.T <span class="badge badge-secondary">Admin</span></h5>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Usuarios
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Nuevo Usuario</a>
                            <a class="dropdown-item" href="adminListar.aspx">Listar Usuarios</a>
                            <a class="dropdown-item" href="adminEditar.aspx">Editar Usuarios</a>
                            <a class="dropdown-item" href="adminEliminar.aspx">Eliminar Usuarios</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Reportes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#"> Reporte por Temporada </a>
                            <a class="dropdown-item" href="#"> Reporte de Comentarios </a>
                            <a class="dropdown-item" href="#"> Reporte de Tendencias </a>
                            <a class="dropdown-item" href="#"> Reporte de "Me Gusta"</a>
                            <a class="dropdown-item" href="#"> Reporte de Favoritos </a>
                            <a class="dropdown-item" href="adminReporteSitio.aspx"> Reporte de Sitios Turisticos </a>
                            <a class="dropdown-item" href="adminReporteEmpresas.aspx"> Reporte de Empresas </a>
                            <a class="dropdown-item" href="adminReporteVisita.aspx"> Reporte de Visitas Técnicas </a>
                            <a class="dropdown-item" href="adminReporteHotel.aspx"> Reporte de Hoteles</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cuenta
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.aspx">Cerrar Sesion</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        
        

        <form class="dd col-12 col-md-8 col-xl-5 mx-auto col-xs-12" runat="server">        
            <asp:Literal ID="escribirHTML" runat="server" ></asp:Literal>
            <h2 class="text-center"><span class="badge badge-secondary">Nuevo Usuario</span> </h2>
            <br />
            <div class="form-group">
                <label for="inputDPI">Documento de Identificacion (*)</label>
                <input type="number" class="form-control" id="inputDPI" placeholder="1234567891234" runat="server">
            </div>
            <div class="form-group">
                <label for="inputNombre">Nombre Completo (*)</label>
                <input type="text" class="form-control" id="inputNombre" placeholder="Eden Hazard" runat="server">
            </div>
            <div class="form-group">
                <label for="inputCorreo">Correo Electronico (*)</label>
                <input type="email" class="form-control" id="inputCorreo" placeholder="name@example.com" runat="server">
            </div>
            <div class="form-group">
                <label for="inputTelefono">Telefono (*)</label>
                <input type="number" class="form-control" id="inputTelefono" placeholder="(502) 50501313" runat="server">
            </div>
            <div class="form-group">
                <label for="inputTelefono">Usuario (*)</label>
                <input type="text" class="form-control" id="inputUsuario" placeholder="edenHzd" runat="server">
            </div>
            <div class="form-group">
                <label for="inputContrasenia">Contraseña (*)</label>
                <input type="password" class="form-control" id="inputContrasenia" placeholder="Tu Contraseña" runat="server">
            </div>
            <div class="form-group">
                <label for="inputRol">Permisos (*)</label>
                <select class="form-control" id="inputRol" runat="server">
                    <option value ="0" disabled="disabled" selected="selected">Rol </option>
                    <option value="1">Administrador</option>
                    <option value="3">Agente Turistico</option>
                    <option value="2">Tecnico Turistico</option>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-outline-secondary btn-lg" runat="server" id ="btnRegistrar" onserverclick="btnRegistrar_Click">
                    Registrar
                </button>
            </div>
        </form>


    </div>



        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>
    
</body>
</html>