﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agenteNuevoSitioTuristico.aspx.cs" Inherits="appProyecto2.agenteNuevoSitioTuristico" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agente Turistico</title>
<!--===============================================================================================-->	
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <style>
        body {
            background: url("/images/bgAgente.jpg");
            background-size: cover;
            background-attachment:fixed;
        }

        .colorFondo {
            background-color: rgba(0,0,0,0.7);
        }

        .dd {
            padding:25px;
            background: linear-gradient(60deg,rgba(10,50,100,0.7),rgba(0,0,0,0.5));
            margin-top: 50px;
            margin-bottom: 100px;
            color: #fff;
        }

        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        #img-upload{
            width: 100%;
        }

    </style>
</head>
<body>
  
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <a class="navbar-brand" href="agenteInicio.aspx">
                <h5>Instituto N.T <span class="badge badge-primary">Agente</span></h5>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Sitios Turisticos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteNuevoSitioTuristico.aspx">Nuevo Sitio Turistico</a>
                            <a class="dropdown-item" href="agenteListarSitiosTuristicos.aspx">Listar Sitios Turisticos</a>
                            <a class="dropdown-item" href="agenteEliminarSitioTuristico.aspx">Eliminar Sitio Turistico</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Empresas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteVerSolicitudes.aspx">Ver Solicitudes</a>
                            <a class="dropdown-item" href="agenteVerificarDatos.aspx">Verificar Datos de Empresa</a>
                            <a class="dropdown-item" href="agenteAsignarVisitaTecnica.aspx">Asignar Visita Tecnica</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Reportes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteReporteSitiosTuristicosInscritos.aspx">Reporte de Sitios Turisticos </a>
                            <a class="dropdown-item" href="agenteReporteSolicitudesEmpresas.aspx">Reporte de Empresas</a>
                            <a class="dropdown-item" href="agenteReporteVisitasTecnicasAsignadas.aspx">Reporte de Visitas Tecnicas Asignadas</a>
                            <a class="dropdown-item" href="agenteReporteEmpresasTipoHotel.aspx">Reporte de servicios de Hotel</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cuenta
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.aspx">Cerrar Sesion</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav> 
        
        <form class="dd col-12 col-md-8 col-xl-4 mx-auto col-xs-12" runat="server">        
            <asp:Literal ID="escribirHTML" runat="server" ></asp:Literal>
            <h2 class="text-center"><span class="badge badge-secondary">Agregar Sitio Turistico</span> </h2>
            <br />
            <div class="form-group">
                <label for="inputDPI">Codigo de Identificacion (*)</label>
                <input type="number" class="form-control" id="inputCodigo" placeholder="1234567891234" runat="server">
            </div>
            <div class="form-group">
                <label for="inputNombre">Nombre del Sitio Turistico (*)</label>
                <input type="text" class="form-control" id="inputNombre" placeholder="Parque de Ejemplo" runat="server">
            </div>
            <div class="form-group">
                <label for="inputCorreo">Descripcion (*)</label>
                <input type="text" class="form-control" id="inputDescripcion" placeholder="Breve Descripcion" runat="server">
            </div>
            <div class="form-group">
                <label for="inputRegion">Region donde se Ubica (*)</label>
                <select class="form-control" id="inputRegion" runat="server">
                    <option value ="0" disabled="disabled" selected="selected">Region </option>
                    <option value="1">Norte</option>
                    <option value="2">Sur</option>
                    <option value="3">Este</option>
                    <option value="4">Oeste</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputCorreo">Fotografia (*)</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-default btn-file">
                            Buscar… <asp:FileUpload ID="imgInp" runat="server" />
                        </span>
                    </span>
                    <input type="text" class="form-control" readonly>
                </div>
                <img id='img-upload'/>
            </div>
            <div class="form-group">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Agregar Sitio" class="btn btn-outline-secondary btn-lg"/>
            </div>
        </form>


    </div>




        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>


    <script>
        $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		}); 	
	});

    </script>
    
</body>
</html>