﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class agenteAsignarVisitaTecnica : System.Web.UI.Page
	{


		MySqlConnection con = new MySqlConnection();
		MySqlConnection conInsert = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlCommand comInsert = new MySqlCommand();
		MySqlDataReader dr,dr2;

		protected void Page_Load(object sender, EventArgs e)
		{

			connectionString();
			con.Open();
			conInsert.Open();
			com.Connection = con;
			comInsert.Connection = conInsert;
			com.CommandText = "SELECT e.codigo, e.nombre, e.direccion, e.telefono,e.correo,r.region,es.estado,t.tipo " +
				"FROM empresa e, region r, estado es, tipo t " +
				"WHERE r.codigo = e.region AND es.codigo = e.estado AND t.codigo = e.tipo AND es.estado = 'en evaluacion';";
			comInsert.CommandText = "SELECT * FROM usuario WHERE rol = 2";
			dr = com.ExecuteReader();
			dr2 = comInsert.ExecuteReader();


			String tecnicos = "<option value= \"0\" disabled selected> SELECCIONE UN TECNICO </option>";
			if (dr2.HasRows) {
				while (dr2.Read()) {
					tecnicos += "<option value=\""+dr2.GetString(0)+"\" >"+dr2.GetString(1)+"</option>";
				}
			}
			conInsert.Close();


			if (dr.HasRows) {
				while (dr.Read()) {
					escribirHTML.Text += "" +
						"<tr class=\"table-dark\">" +
						"	<td scope='row'>" + dr.GetString(0) + "</td>" +
						"	<td>" + dr.GetString(1) + "</td>" +
						"	<td><select class=\"form-control\" id=\"idTecnico\" >" + tecnicos + "</select></td>" +
						"<td><button type=\"button\" class=\"btn btn-warning btn-delete\" data-toggle=\"modal\" data-target=\"#exampleModal\">Asignar Tecnico</button></td>" +
						"</tr>";
				}
			}
			con.Close();

		}

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
			conInsert.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}


		protected void Button1_Click1(object sender, EventArgs e)
		{
			string dpiTecnico = idTecnicoAsignado.Value;
			string registro = idRegistro.Value;

			connectionString();
			con.Open();
			conInsert.Open();
			com.Connection = con;
			comInsert.Connection = conInsert;

			if (dpiTecnico == "0") {
				Response.AddHeader("REFRESH", "3;URL=agenteAsignarVisitaTecnica.aspx");
				escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
			} else {
				com.CommandText = "SELECT COUNT(*) FROM visitaTecnica WHERE empresa = "+registro+";";
				dr = com.ExecuteReader();

				if (dr.HasRows) {
					while (dr.Read()) {
						if (dr.GetString(0) == "0") {
							comInsert.CommandText = "INSERT INTO visitaTecnica VALUES("+dpiTecnico+","+registro+");" +
								"UPDATE empresa SET estado = 3 WHERE codigo = " + registro + ";";
							comInsert.ExecuteReader();
							escribirHTML.Text = "<div class=\"alert alert-success\" role=\"alert\">Tecnico Asignado!</div>";
							conInsert.Close();
						} else {
							Response.AddHeader("REFRESH", "3;URL=agenteAsignarVisitaTecnica.aspx");
							escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">Ya cuenta con un tecnico asignado!</div>";
						}
					}
				}
			}
		}
	}
}