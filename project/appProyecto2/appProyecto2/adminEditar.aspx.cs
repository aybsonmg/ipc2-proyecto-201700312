﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class adminEditar : System.Web.UI.Page
	{

		MySqlConnection con = new MySqlConnection();
		MySqlConnection conInsert = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlCommand comInsert = new MySqlCommand();
		MySqlDataReader dr;


		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT u.dpi,u.nombre,u.correo,u.telefono,u.usuario,u.contrasenia,r.rol FROM usuario u, rol r WHERE u.rol = r.codigo;";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					escribirHTML.Text += "" +
						"<tr class=\"table-dark\">" +
						"	<td scope='row'>" + dr.GetString(0) + "</td>" +
						"	<td>" + dr.GetString(1) + "</td>" +
						"	<td>" + dr.GetString(2) + "</td>" +
						"	<td>" + dr.GetString(3) + "</td>" +
						"	<td>" + dr.GetString(4) + "</td>" +
						"	<td>" + dr.GetString(5) + "</td>" +
						"	<td>" + dr.GetString(6) + "</td>" +
						"<td><button type=\"button\" class=\"btn btn-warning btn-delete\" data-toggle=\"modal\" data-target=\"#exampleModal\">Editar</button></td>" +
						"</tr>";// +
								//"<script>document.getElementById(\"btn1\").addEventListener(\"click\", function(document.getElementById(\"pr\").innerHTML = \"Hello World\";});</ script > ";
				}
			}
			con.Close();
		}

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
			conInsert.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}



		protected void Button1_Click1(object sender, EventArgs e)
		{
			string dpi = idDPI.Value;
			string nombre = idNombre.Value;
			string correo = idCorreo.Value;
			string telefono = idTelefono.Value;
			string contrasenia = idPass.Value;

			if (dpi != "1") {

				connectionString();
				con.Open();
				conInsert.Open();
				com.Connection = con;
				comInsert.Connection = conInsert;

				if (dpi == "" || nombre == "" || correo == "" || telefono == "" || contrasenia == "") {
					escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
				} else {


					comInsert.CommandText = "UPDATE usuario SET nombre = \""+nombre+"\", telefono = "+telefono+" ,correo = \""+correo+"\" , contrasenia = \""+contrasenia+"\" WHERE dpi = "+dpi+";";
					comInsert.ExecuteReader();
					Literal2.Text = "<div class=\"alert alert-success\" role=\"alert\">Usuario Registrado con Exito!</div>";
					Response.AddHeader("REFRESH", "1;URL=adminEditar.aspx");
					conInsert.Close();

				}
			} 
		}
	}
}