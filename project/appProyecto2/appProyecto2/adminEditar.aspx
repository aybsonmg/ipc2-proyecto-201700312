﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminEditar.aspx.cs" Inherits="appProyecto2.adminEditar" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administrador</title>
<!--===============================================================================================-->	
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <style>
        body {
            background: url("/images/bg.jpg");
            background-size: cover;
            background-attachment:fixed;
        }

        .dd {
            margin-top:40px;
        }
    </style>
</head>
<body>
  <form runat="server">
  
    <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
            <a class="navbar-brand" href="#">
                <h5>Instituto N.T <span class="badge badge-secondary" id="pr">Admin</span></h5>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Usuarios
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="adminRegistro.aspx">Nuevo Usuario</a>
                            <a class="dropdown-item" href="adminListar.aspx">Listar Usuarios</a>
                            <a class="dropdown-item" href="#">Editar Usuarios</a>
                            <a class="dropdown-item" href="adminEliminar.aspx">Eliminar Usuarios</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Reportes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#"> Reporte por Temporada </a>
                            <a class="dropdown-item" href="#"> Reporte de Comentarios </a>
                            <a class="dropdown-item" href="#"> Reporte de Tendencias </a>
                            <a class="dropdown-item" href="#"> Reporte de "Me Gusta"</a>
                            <a class="dropdown-item" href="#"> Reporte de Favoritos </a>
                            <a class="dropdown-item" href="adminReporteSitio.aspx"> Reporte de Sitios Turisticos </a>
                            <a class="dropdown-item" href="adminReporteEmpresas.aspx"> Reporte de Empresas </a>
                            <a class="dropdown-item" href="adminReporteVisita.aspx"> Reporte de Visitas Técnicas </a>
                            <a class="dropdown-item" href="adminReporteHotel.aspx"> Reporte de Hoteles</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cuenta
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.aspx">Cerrar Sesion</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        
        <div class="dd col-12 col-md-8 col-xl-8 mx-auto col-xs-12 dd table-responsive">
            <table class="table">
                <thead class="thead-dark bg-light">
                    <tr>
                        <th scope="col">DPI</th>
                        <th scope="col">NOMBRE COMPLETO</th>
                        <th scope="col">CORREO ELECTRONICO</th>
                        <th scope="col">TELEFONO</th>
                        <th scope="col">USUARIO</th>
                        <th scope="col">CONTRASEÑA</th>
                        <th scope="col">ROL</th>
                        <th scope="col"> </th>
                    </tr>
                </thead>
               <tbody>
                   <asp:Literal ID="escribirHTML" runat="server"></asp:Literal>                   
               </tbody>
            </table>
        </div>

            
                
    </div>

    <!-- Modal -->
<%--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">¿Desea eliminar el usuario?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="text" id="idEliminar" runat="server" />
          <%--<asp:Literal ID="Literal1" runat="server"><p id="idEliminar"></p></asp:Literal>  --%>        
    <%--      <p id="nombreEliminar"></p>         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
<%--        <button type="button" class="btn btn-primary" runat="server" data-dismiss="modal" id ="btnEliminar" onserverclick="btnEliminar_Click">Eliminar</button>--%>
 <%--       <asp:Button ID="Button1" class="btn btn-primary" runat="server" OnClick="Button1_Click" Text="Modificar" />
      </div>
    </div>
  </div>
<%--</div>--%>


        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
          <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Documento de Identificación</label>
            <input type="text" class="form-control" id="idDPI" runat="server" readonly/>
          </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Nombre Completo:</label>
                <input type="text" class="form-control" id="idNombre" runat="server"/>
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Correo Electronico:</label>
                <input type="email" class="form-control" id="idCorreo" runat="server"/>
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Telefono:</label>
                <input type="text" class="form-control" id="idTelefono" runat="server"/>
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Usuario:</label>
                <input type="text" class="form-control" id="idUsuario" readonly runat="server"/>
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Contraseña:</label>
                <input type="text" class="form-control" id="idPass" runat="server"/>
            </div>
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <asp:Button ID="Button1" runat="server" Text="Button" class="btn btn-primary" OnClick="Button1_Click1" />
      </div>
    </div>
  </div>
</div>
    
        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>

    <script>
        //document.getElementById("botonEliminar").addEventListener("click", function(){
        //    document.getElementById("pr").innerHTML = "Hello World";
        //});

        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var id = $(this).parent().parent().children().first().text();
            var nom = $(this).parent().parent().children().first().next().text();
            var cor = $(this).parent().parent().children().first().next().next().text();
            var tel = $(this).parent().parent().children().first().next().next().next().text();
            var usr = $(this).parent().parent().children().first().next().next().next().next().text();
            var pss = $(this).parent().parent().children().first().next().next().next().next().next().text();
            var rol = $(this).parent().parent().children().first().next().next().next().next().next().next().next().next().text();
            console.log(id);
            $("#idDPI").val(id);
            $("#idNombre").val(nom);
            $("#idCorreo").val(cor);
            $("#idTelefono").val(tel);
            $("#idUsuario").val(usr);
            $("#idPass").val(pss);
            //$("#idRol").val(rol);
        });

    </script>
                
  
    
       </form>
    
    
    
                
  
    
       
    
    
</body>
</html>