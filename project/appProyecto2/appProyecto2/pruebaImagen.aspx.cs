﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;


namespace appProyecto2
{
	public partial class pruebaImagen : System.Web.UI.Page
	{

		MySqlConnection con = new MySqlConnection();
		MySqlConnection conInsert = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlCommand comInsert = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = prueba; Password = realmadrid12";
			conInsert.ConnectionString = "Server = localhost; UserID = root; Database = prueba; Password = realmadrid12";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
		}

		protected void Button1_Click(object sender, EventArgs e)
		{

			try {
				if (FileUpload1.HasFile) {
					string extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
					if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png") {
						Literal1.Text = "<h1>Bytes: " + Server.MapPath(FileUpload1.FileName) + "</h1>";
						MemoryStream ms = new MemoryStream(FileUpload1.FileBytes);
						byte[] imageByteArray = ms.ToArray();

						connectionString();
						con.Open();
						com.Connection = con;

						using (var cmd = new MySqlCommand("INSERT INTO img SET img = @image",
											  con)) {
							cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = imageByteArray;
							cmd.ExecuteNonQuery();
						}
						con.Close();
					} else {
						Literal1.Text = "<h1>Debe de subir una imagen con formato .png, .jpg o .jpeg</h1>";
					}
				} else {
					Literal1.Text = "<h1>Debe de subir una imagen con formato .png, .jpg o .jpeg</h1>";
				}
			} catch (Exception) {
				Literal1.Text = "<h1>Debe de subir una imagen más pequeña</h1>";
			}

			


		}

		protected void Button2_Click(object sender, EventArgs e)
		{
			
			connectionString();
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT img FROM img WHERE id = 3;";
			

			
				byte[] image = (byte[]) com.ExecuteScalar();

				string strBase64 = Convert.ToBase64String(image);

				Image1.ImageUrl = "data:Image/png;base64,"+ strBase64;
				
			
			con.Close();

			
			


		}
	}
}