﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminReporteEmpresas.aspx.cs" Inherits="appProyecto2.adminReporteEmpresas" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administrador</title>
<!--===============================================================================================-->	
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <style>
        body {
            background: url("./images/bg.jpg");
            background-size: cover;
            background-attachment:fixed;
        }

        .colorFondo {
            background-color: rgba(0,0,0,0.7);
        }

        .dd {
            padding:25px;
            background: linear-gradient(60deg,rgba(10,50,100,0.7),rgba(0,0,0,0.5));
            margin-top: 50px;
            margin-bottom: 100px;
            color: #fff;
        }

   

    </style>
</head>
<body>
  
    <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
            <a class="navbar-brand" href="#">
                <h5>Instituto N.T <span class="badge badge-secondary">Admin</span></h5>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Usuarios
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Nuevo Usuario</a>
                            <a class="dropdown-item" href="adminListar.aspx">Listar Usuarios</a>
                            <a class="dropdown-item" href="adminEditar.aspx">Editar Usuarios</a>
                            <a class="dropdown-item" href="adminEliminar.aspx">Eliminar Usuarios</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Reportes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#"> Reporte por Temporada </a>
                            <a class="dropdown-item" href="#"> Reporte de Comentarios </a>
                            <a class="dropdown-item" href="#"> Reporte de Tendencias </a>
                            <a class="dropdown-item" href="#"> Reporte de "Me Gusta"</a>
                            <a class="dropdown-item" href="#"> Reporte de Favoritos </a>
                            <a class="dropdown-item" href="adminReporteSitio.aspx"> Reporte de Sitios Turisticos </a>
                            <a class="dropdown-item" href="adminReporteEmpresas.aspx"> Reporte de Empresas </a>
                            <a class="dropdown-item" href="adminReporteVisita.aspx"> Reporte de Visitas Técnicas </a>
                            <a class="dropdown-item" href="adminReporteHotel.aspx"> Reporte de Hoteles</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cuenta
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.aspx">Cerrar Sesion</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        
        <div class="container col-xl-12 mx-auto row m-4 d-flex justify-content-center">
            
            <div class="card mb-3 col-xl-2 p-2 m-4">
                <img class="card-img-top" src="imgInicio/screen/2.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Reporte Region</h5>
                    <p class="card-text">Region Norte = <asp:Literal ID="numeroEmpresasNorte" runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">Region Sur   = <asp:Literal ID="numeroEmpresasSur"   runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">Region Este  = <asp:Literal ID="numeroEmpresasEste"  runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">Region Oeste = <asp:Literal ID="numeroEmpresasOeste" runat="server"></asp:Literal> empresas </p>
                    <p class="card-text"><small class="text-muted">Reporte de Empresas por Region</small></p>
                </div>
            </div>

            <div class="card mb-3 col-xl-2 p-2 m-4">
                <img class="card-img-top" src="imgInicio/screen/4.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Reporte Tipo</h5>
                    <p class="card-text">Restaurante = <asp:Literal ID="Literal1" runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">Hotel       = <asp:Literal ID="Literal2"   runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">Museo       = <asp:Literal ID="Literal3"  runat="server"></asp:Literal> empresas </p>
                    <p class="card-text"><small class="text-muted">Reporte de Tipos de Empresas</small></p>
                </div>
            </div>

            <div class="card mb-3 col-xl-2 p-2 m-4">
                <img class="card-img-top" src="imgInicio/screen/8.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Reporte Estado</h5>
                    <p class="card-text">Pre-evaluacion = <asp:Literal ID="Literal4"  runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">En evaluacion  = <asp:Literal ID="Literal5"  runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">Visita Tecnica = <asp:Literal ID="Literal6"  runat="server"></asp:Literal> empresas </p>
                    <p class="card-text">Inscrito       = <asp:Literal ID="Literal7"  runat="server"></asp:Literal> empresas </p>
                    <p class="card-text"><small class="text-muted">Reporte de Empresas por Estado</small></p>
                </div>
            </div>


        </div>

       


    </div>



        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>
    
</body>
</html>