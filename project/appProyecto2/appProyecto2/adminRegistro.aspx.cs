﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class adminRegistro : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlConnection conInsert = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlCommand comInsert = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
			conInsert.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
		}

		protected void btnRegistrar_Click(object sender, EventArgs e)
		{

			string dpi = inputDPI.Value;
			string nombre = inputNombre.Value;
			string correo = inputCorreo.Value;
			string telefono = inputTelefono.Value;
			string usuario = inputUsuario.Value;
			string contrasenia = inputContrasenia.Value;
			string rol = inputRol.Value;

			connectionString();
			con.Open();
			conInsert.Open();
			com.Connection = con;
			comInsert.Connection = conInsert;

			if (dpi == "" || nombre == "" || correo == "" || telefono == "" || usuario == "" || contrasenia == "" || rol == "0") {
				Response.AddHeader("REFRESH", "1;URL=adminRegistro.aspx#contactUser");
				escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
			} else {
				com.CommandText = "SELECT COUNT(*) FROM usuario WHERE dpi='" + dpi + "' OR usuario='" + usuario + "';";
				dr = com.ExecuteReader();

				if (dr.HasRows) {
					while (dr.Read()) {
						if (dr.GetString(0) == "0") {
							comInsert.CommandText = "INSERT INTO usuario VALUES(" +
								dpi + ",'" + nombre + "','" + correo + "'," + telefono + ",'" + usuario + "','" + contrasenia + "'," + rol + ");";
							comInsert.ExecuteReader();							
							Response.AddHeader("REFRESH", "1;URL=adminRegistro.aspx#contactUser");
							escribirHTML.Text = "<div class=\"alert alert-success\" role=\"alert\">Usuario Registrado con Exito!</div>";
							conInsert.Close();
						} else {
							Response.AddHeader("REFRESH", "1;URL=adminRegistro.aspx#contactUser");
							escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">El usuario o el DPI que esta tratando de registrar, ya existe!</div>";
						}
					}
				}
			}
		}
	}
}
