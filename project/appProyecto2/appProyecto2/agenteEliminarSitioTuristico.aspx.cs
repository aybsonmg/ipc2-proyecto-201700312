﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class agenteEliminarSitioTuristico : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT s.codigo, s.nombre, s.descripcion, r.region FROM sitio_turistico s, region r WHERE r.codigo = s.region;";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					escribirHTML.Text += "" +
						"<tr class=\"table-dark\">" +
						"	<td scope='row'>" + dr.GetString(0) + "</td>" +
						"	<td>" + dr.GetString(1) + "</td>" +
						"	<td>" + dr.GetString(2) + "</td>" +
						"	<td>" + dr.GetString(3) + "</td>" +
						"<td><button type=\"button\" class=\"btn btn-danger btn-delete\" data-toggle=\"modal\" data-target=\"#exampleModal\">Eliminar</button></td>" +
						"</tr>";// +
								//"<script>document.getElementById(\"btn1\").addEventListener(\"click\", function(document.getElementById(\"pr\").innerHTML = \"Hello World\";});</ script > ";
				}
			}
			con.Close();
		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			string id = idEliminar.Value;

			try {
				connectionString();
				con.Open();
				com.Connection = con;
				com.CommandText = "DELETE FROM fotografia_sitio WHERE sitio_turistico = " + id + ";" +
					"DELETE FROM sitio_turistico WHERE codigo = " + id + ";";
				com.ExecuteNonQuery();
				con.Close();
				Response.Redirect("agenteEliminarSitioTuristico.aspx");
			} catch (Exception) { }

			
			
			


		}

	}
}