﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class agenteVerSolicitudes : System.Web.UI.Page
	{

		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;

		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT e.codigo, e.nombre, e.direccion, e.telefono,e.correo,r.region,es.estado,t.tipo " +
				"FROM empresa e, region r, estado es, tipo t " +
				"WHERE r.codigo = e.region AND es.codigo = e.estado AND t.codigo = e.tipo AND es.estado = 'pre-evaluacion';";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					escribirHTML.Text += "" +
						"<tr class=\"table-dark\">" +
						"	<td scope='row'>" + dr.GetString(0) + "</td>" +
						"	<td>" + dr.GetString(1) + "</td>" +
						"	<td>" + dr.GetString(2) + "</td>" +
						"	<td>" + dr.GetString(3) + "</td>" +
						"	<td>" + dr.GetString(4) + "</td>" +
						"	<td>" + dr.GetString(5) + "</td>" +
						"	<td>" + dr.GetString(6) + "</td>" +
						"	<td>" + dr.GetString(7) + "</td>" +
						"</tr>";
				}
			}
			con.Close();
		}

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}
	}
}