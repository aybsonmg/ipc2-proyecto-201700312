﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class agenteNuevoSitioTuristico : System.Web.UI.Page
	{

		MySqlConnection con = new MySqlConnection();
		MySqlConnection conInsert = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlCommand comInsert = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
			conInsert.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			
		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			connectionString();
			String codigo = inputCodigo.Value;
			String nombre = inputNombre.Value;
			String descripcion = inputDescripcion.Value;
			String region = inputRegion.Value;


			if (codigo == "" || nombre == "" || descripcion == "" || region == "0" || !imgInp.HasFile) {
				escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
			} else {

				conInsert.Open();
				comInsert.Connection = conInsert;
				comInsert.CommandText = "SELECT COUNT(*) FROM sitio_turistico WHERE codigo = "+codigo+";";
				dr = comInsert.ExecuteReader();

				if (dr.HasRows) {
					while (dr.Read()) {
						if (dr.GetString(0) == "0") {
							

								string extension = Path.GetExtension(imgInp.PostedFile.FileName);
								if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png") {
									MemoryStream ms = new MemoryStream(imgInp.FileBytes);
									byte[] imageByteArray = ms.ToArray();
								
									con.Open();
									com.Connection = con;

									using (var cmd = new MySqlCommand("INSERT INTO sitio_turistico VALUES ("+codigo+",'"+nombre+"','"+descripcion+"',"+region+");" +
										"INSERT INTO fotografia_sitio VALUES (@image,@codigoSitio);",
														  con)) {
										cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = imageByteArray;
										cmd.Parameters.Add("@codigoSitio", MySqlDbType.Int32).Value = codigo;
										cmd.ExecuteNonQuery();
									}
									con.Close();
									escribirHTML.Text = "<div class=\"alert alert-success\" role=\"alert\">Usuario Registrado con Exito!</div>";
								Response.AddHeader("REFRESH", "3;URL=agenteNuevoSitioTuristico");
							} else {
									escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de subir una imagen con formato .png, .jpg o .jpeg!</div>";
								}

							
						} else {
							escribirHTML.Text = "<div class=\"alert alert-danger\" role=\"alert\">El sitio que esta tratando de registrar, ya existe!</div>";
						}
					}
				}

			}


			

		}
	}
}