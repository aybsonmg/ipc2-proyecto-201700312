﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class agenteVerificarDatos : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlConnection conInsert = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlCommand comInsert = new MySqlCommand();
		MySqlDataReader dr;

		protected void Page_Load(object sender, EventArgs e)
		{			
			connectionString();
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT e.codigo, e.nombre, e.direccion, e.telefono,e.correo,r.region,es.estado,t.tipo " +
				"FROM empresa e, region r, estado es, tipo t " +
				"WHERE r.codigo = e.region AND es.codigo = e.estado AND t.codigo = e.tipo AND es.estado = 'pre-evaluacion';";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					escribirHTML.Text += "" +
						"<tr class=\"table-dark\">" +
						"	<td scope='row'>" + dr.GetString(0) + "</td>" +
						"	<td>" + dr.GetString(1) + "</td>" +
						"	<td>" + dr.GetString(2) + "</td>" +
						"	<td>" + dr.GetString(3) + "</td>" +
						"	<td>" + dr.GetString(4) + "</td>" +
						"	<td>" + dr.GetString(5) + "</td>" +
						"	<td>" + dr.GetString(6) + "</td>" +
						"	<td>" + dr.GetString(7) + "</td>" +
						"<td><button type=\"button\" class=\"btn btn-warning btn-delete\" data-toggle=\"modal\" data-target=\"#exampleModal\">Verificar Datos</button></td>" +
						"</tr>";
				}
			}
			con.Close();


		}



		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
			conInsert.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}


		protected void Button1_Click1(object sender, EventArgs e) {

			string registro = idRegistro.Value;
			string nombre = idNombre.Value;
			string direccion = idDireccion.Value;
			string telefono = idTelefono.Value;
			string correo = idCorreo.Value;
			string auxregion = idRegion.Value;
			string region = "";
			string estado = "2";
			string auxtipoEmpresa = idTipo.Value ;
			string tipoEmpresa = "";


			if (auxregion == "norte") {
				region = "1";
			} else if (auxregion == "sur") {
				region = "2";
			} else if (auxregion == "este") {
				region = "3";
			} else if (auxregion == "oeste") {
				region = "4";
			}


			if (auxtipoEmpresa == "restaurante") {
				tipoEmpresa = "1";
			} else if (auxtipoEmpresa == "hotel") {
				tipoEmpresa = "2";
			} else if (auxtipoEmpresa == "museo") {
				tipoEmpresa = "3";
			}

			connectionString();
			con.Open();
			conInsert.Open();
			com.Connection = con;
			comInsert.Connection = conInsert;

			if (registro == "" || nombre == "" || direccion == "" || telefono == "" || correo == "" || region == "" || tipoEmpresa == "" ) {
					Literal2.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
			} else {
				comInsert.CommandText = "UPDATE empresa SET nombre = '" + nombre + "', direccion = '" + direccion + "' ,telefono = " + telefono + " , correo = '" + correo + "', region = "+region+", estado = 2, tipo = " + tipoEmpresa + " " +
					"WHERE codigo = " + registro + ";";
				Literal2.Text = "<script>alert('Se confirmaron los datos de " + nombre + ", ahora asigne una visita tecnica a la empresa!')</script>";
				comInsert.ExecuteReader();
				Response.AddHeader("REFRESH", "0;URL=agenteVerificarDatos.aspx");								
				conInsert.Close();
			}
		
		}


	}
}