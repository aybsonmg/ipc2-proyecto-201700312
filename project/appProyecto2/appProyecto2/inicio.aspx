﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inicio.aspx.cs" Inherits="appProyecto2.inicio" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
  <meta charset="utf-8">
  <title>INAT</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="imgInicio/favicon.png" rel="icon">
  <link href="imgInicio/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap css -->
  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link href="libInicio/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="libInicio/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="libInicio/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
  <link href="libInicio/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="libInicio/animate/animate.min.css" rel="stylesheet">
  <link href="libInicio/modal-video/css/modal-video.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="cssInicio/style.css" rel="stylesheet">
</head>

<body>

    <form id="form1" runat="server">

  <header id="header" class="header header-hide">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto"><span>I</span>NAT</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#hero">Inicio</a></li>
          <li><a href="#about-us">Nosotros</a></li>
          <li><a href="#features">Recomendaciones</a></li>
          <li><a href="#screenshots">Fotografias</a></li>
          <li><a href="#blog">Buscar Sitio</a></li>          
          <li class="menu-has-children"><a href="">Registro</a>
            <ul>
              <li><a href="#contactUser">Usuario</a></li>
              <li><a href="#contact">Empresa</a></li>
            </ul>
          </li>
            <li><a href="#testimonials">Comentarios</a></li>
          <li><a href="index.aspx">Empleados</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Hero Section
  ============================-->
  <section id="hero" class="wow fadeIn">
    <div class="hero-container">
      <h1>Bienvenido a Guatemala</h1>
      <h2>El pais de la eterna primavera</h2>
      <img src="imgInicio/hero-img.png" alt="Hero Imgs">
      <a href="#about-us" class="btn-get-started scrollto">¿Quien Somos?</a>
    </div>
  </section><!-- #hero -->

  <!--==========================
    About Us Section
  ============================-->
  <section id="about-us" class="about-us padd-section wow fadeInUp">
    <div class="container">
      <div class="row justify-content-center">

        

        <div class="col-md-7 col-lg-5">
          <div class="about-content">

            <h2><span>INAT</span>¿Quien Somos?</h2>           
            <ul class="list-unstyled">
              <li><i class="fa fa-angle-right"></i>Mision</li>
              <p class="text-justify">Somos la autoridad superior en materia de turismo en Guatemala, que rige y controla la promoción, fomento y desarrollo sostenible de la industria turística, 
                  en el marco de la legislación y planificación sectorial y de su coordinación entre los sectores público, privado y sociedad civil                  
              </p>
              <li><i class="fa fa-angle-right"></i>Vision</li>
              <p class="text-justify">Para el 2025, el INGUAT se ha consolidado como el líder que coordina eficazmente, el desarrollo y promoción turística del país, con las instituciones públicas
                 y privadas, en el marco del Plan Maestro de Turismo Sostenible de Guatemala 2015-2025                 
              </p>
              <li><i class="fa fa-angle-right"></i>Objetivo</li>
              <p class="text-justify">Consolidar al turismo como eje articulador del desarrollo económico y social de Guatemala en el marco de la sostenibilidad, de forma que contribuya a generar
                 las condiciones necesarias para mejorar la competitividad del país en el ámbito internacional y a favorecer el acceso de los guatemaltecos a una vida digna                 
               </p>
            </ul>

          </div>
        </div>

        <div class="col-md-5 col-lg-3">
            <img src="imgInicio/about-img.png" alt="About">
          </div>

      </div>
    </div>
  </section>

  <!--==========================
    Features Section
  ============================-->

  <section id="features" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Sitios Recomendados</h2>
        <p class="separator">Sitios Turisticos que debes de visitar si o si, en tu estadia en Guatemala</p>
      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <img src="imgInicio/svg/paint-palette.svg" alt="img" class="img-fluid">
            <h4>Parque Nacional Tikal</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <img src="imgInicio/svg/vector.svg" alt="img" class="img-fluid">
            <h4>Lago de Atitlan</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <img src="imgInicio/svg/design-tool.svg" alt="img" class="img-fluid">
            <h4>Arco de Santa Catalina</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <img src="imgInicio/svg/asteroid.svg" alt="img" class="img-fluid">
            <h4>Mercado de Chichicastenango</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <img src="imgInicio/svg/cloud-computing.svg" alt="img" class="img-fluid">
            <h4>Monterrico</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <img src="imgInicio/svg/pixel.svg" alt="img" class="img-fluid">
            <h4>Yaxhá</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
          <div class="feature-block">
            <img src="imgInicio/svg/code.svg" alt="img" class="img-fluid">
            <h4>Río Dulce</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-3">
            <div class="feature-block">
              <img src="imgInicio/svg/asteroid.svg" alt="img" class="img-fluid">
              <h4>Puerto San Jose</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
          </div>

      </div>
    </div>
  </section>

  <!--==========================
    Screenshots Section
  ============================-->
  <section id="screenshots" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Fotografias</h2>
        <p class="separator">Algunas fotografias para que disfrutes</p>
      </div>
    </div>

    <div class="container">
      <div class="owl-carousel owl-theme">

        <div><img src="imgInicio/screen/1.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/2.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/3.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/4.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/5.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/6.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/7.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/8.jpg" alt="img"></div>
        <div><img src="imgInicio/screen/9.jpg" alt="img"></div>

      </div>
    </div>

  </section>

  <!--==========================
    Blog Section
  ============================-->
  <section id="blog" class="padd-section wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">

        <h2>Latest posts</h2>
        <p class="separator">Integer cursus bibendum augue ac cursus .</p>

      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-lg-4">
          <div class="block-blog text-left">
            <a href="#"><img src="imgInicio/blog/blog-image-1.jpg" alt="img"></a>
            <div class="content-blog">
              <h4><a href="#">whats isthe difference between good and bat typography</a></h4>
              <span>05, juin 2017</span>
              <a class="pull-right readmore" href="#">read more</a>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-4">
          <div class="block-blog text-left">
            <a href="#"><img src="imgInicio/blog/blog-image-2.jpg" class="img-responsive" alt="img"></a>
            <div class="content-blog">
              <h4><a href="#">whats isthe difference between good and bat typography</a></h4>
              <span>05, juin 2017</span>
              <a class="pull-right readmore" href="#">read more</a>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-4">
          <div class="block-blog text-left">
            <a href="#"><img src="imgInicio/blog/blog-image-3.jpg" class="img-responsive" alt="img"></a>
            <div class="content-blog">
              <h4><a href="#">whats isthe difference between good and bat typography</a></h4>
              <span>05, juin 2017</span>
              <a class="pull-right readmore" href="#">read more</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>


<!--==========================
    Contact Section Usuario
  ============================-->
      <section id="contactUser" class="padd-section wow fadeInUp contactUser">

          <div class="container">
            <div class="section-title text-center">
              <h2>Registro Usuarios Visitantes</h2>
              <p class="separator">Guarda toda la información de tus sitios favoritos, unete a nuestra comunidad</p>
            </div>
          </div>
      
          <div class="container">
            <div class="row justify-content-center">
                     
              <div class="col-lg-5 col-md-8">
                <div class="form">
                   <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                  <div id="sendmessage">Your message has been sent. Thank you!</div>
                  <div id="errormessage"></div>
                    <div class="form-group">
                      <input runat="server" type="text" name="name" class="form-control" id="nombreUsuario" placeholder="Nombre Completo" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <input runat="server" type="email" class="form-control" name="email" id="emailUsuario" placeholder="Tu Correo Electronico" data-rule="email"  />
                    </div>
                    <div class="form-group">
                      <input runat="server" type="text" class="form-control" name="subject" id="idUsr" placeholder="ID Usuario" data-rule="minlen:4" data-msg="Ingrese un usuario, minimo 4 caracteres." />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group">
                      <input runat="server" type="password" class="form-control" name="subject" id="idPass" placeholder="Contraseña" data-rule="minlen:3" data-msg="Ingrese una contraseña de 3 caracteres min." />
                      <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit" runat="server" id ="btnRegistrar" onserverclick="btnRegistrar_Click">Registrar</button></div>
                </div>
              </div>
            </div>
          </div>
        </section><!-- #contact -->
  
  


  <!--==========================
    Testimonials Section
  ============================-->

  <section id="testimonials" class="padd-section text-center wow fadeInUp">
      <div class="container">
        <div class="row justify-content-center">
  
          <div class="col-md-8">
  
            <div class="testimonials-content">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  
                <div class="carousel-inner" role="listbox">
  
                  <div class="carousel-item  active">
                    <div class="top-top">
  
                      <h2>Our Users Speack volumes us</h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book. It has survived not only five centuries.</p>
                      <h4>Kimberly Tran<span>manager</span></h4>
  
                    </div>
                  </div>
  
                  <div class="carousel-item ">
                    <div class="top-top">
  
                      <h2>Our Users Speack volumes us</h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book. It has survived not only five centuries.</p>
                      <h4>Henderson<span>manager</span></h4>
  
                    </div>
                  </div>
  
                  <div class="carousel-item ">
                    <div class="top-top">
  
                      <h2>Our Users Speack volumes us</h2>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                        specimen book. It has survived not only five centuries.</p>
                      <h4>David Spark<span>manager</span></h4>
  
                    </div>
                  </div>
  
                </div>
  
                <div class="btm-btm">
  
                  <ul class="list-unstyled carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                  </ul>
  
                </div>
  
              </div>
            </div>
          </div>
  
        </div>
      </div>      
    </section>



  <!--==========================
    Contact Section
  ============================-->
  <section id="contact" class="padd-section wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Solicitud Inscripción Empresa</h2>
        <p class="separator">Completa la informacion que se solicita
          </p>
      </div>
    </div>

    <div class="container">
      <div class="row justify-content-center">

        <div class="col-lg-3 col-md-4">

          <div class="info">
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Nombre de la Empresa</p>
            </div>
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Direccion de la Empresa</p>
            </div>
            
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Telefono de la Empresa</p>
            </div>
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Correo de la Empresa</p>
            </div>
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Ubicacion de la Empresa</p>
            </div>
            <div>
              <i class="fa fa-map-marker"></i>
              <p>Tipo de Empresa</p>
            </div>
          </div>

        </div>

        <div class="col-lg-5 col-md-8">
          <div class="form">
              <asp:Literal ID="Literal2" runat="server"></asp:Literal>
            <div id="sendmessage">Solicitud Enviada!</div>
            <div id="errormessage"></div>
              <div class="form-group">
                  <input runat="server" type="text" name="name" class="form-control" id="registroEmpresa" placeholder="Registro de la Empresa" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
              </div>
              <div class="form-group">
                <input runat="server" type="text" name="name" class="form-control" id="nombreEmpresa" placeholder="Nombre de la Empresa" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                  <input runat="server" type="text" name="name" class="form-control" id="direccionEmpresa" placeholder="Direccion de la Empresa" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
              </div>
              <div class="form-group">
                  <input runat="server" type="number" name="name" class="form-control" id="telefonoEmpresa" placeholder="Telefono de Contacto" data-rule="minlen:1" data-msg="Please enter at least 1 chars" />
                  <div class="validation"></div>
              </div>
              <div class="form-group">
                <input runat="server" type="email" class="form-control" name="email" id="correoEmpresa" placeholder="Correo Electronico de la Empresa" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                  <select class="form-control" data-msg="Seleccione una region" runat="server" id="regionEmpresa">
                      <option value="0" disabled selected>Region</option>
                      <option value="1">Norte</option>
                      <option value="2">Sur</option>
                      <option value="3">Este</option>
                      <option value="4">Oeste</option>
                  </select>
              </div>
              <div class="form-group">
                  <select class="form-control" id="idEmpresa" data-msg="Seleccione un tipo de Empresa" runat="server">
                      <option value="0" disabled selected>Tipo de Empresa</option>
                      <option value="1" >Restaurante</option>
                      <option value="2">Hotel</option>
                      <option value="3">Museo</option>
                  </select>
              </div>
              <div class="form-group">
                  <select class="form-control" id="mostrarInput" data-msg="Seleccione una especialidad" style="display:none;" runat="server">
                      <option value="0" disabled selected>Especialidad Restaurante</option>
                      <option value="comida tipica" >Comida Tipica</option>
                      <option value="helados">Helados</option>
                      <option value="mariscos">Mariscos</option>
                      <option value="cafe">Cafe</option>
                      <option value="carne">Carne</option>
                      <option value="bar">Bar</option>
                  </select>
              </div>
              <div class="form-group">
                  <select class="form-control"  data-msg="Seleccione una especialidad" style="display:none;" id="mostrarInputHotel1" runat="server">
                      <option value="0" disabled selected>Servicio Principal:</option>
                      <option value="piscina" >Piscina</option>
                      <option value="internet">Internet</option>
                      <option value="room service">Room Service</option>
                      <option value="gimnasio">Gimnasio</option>
                      <option value="agua caliente">Agua Caliente</option>
                      <option value="vip">VIP</option>
                  </select>
              </div>
              <div class="form-group">
                  <select class="form-control"  data-msg="Seleccione una especialidad" style="display:none;" id="mostrarInputHotel2" runat="server">
                      <option value="0" disabled selected>Servicio Secundario:</option>
                      <option value="piscina" >Piscina</option>
                      <option value="internet">Internet</option>
                      <option value="room service">Room Service</option>
                      <option value="gimnasio">Gimnasio</option>
                      <option value="agua caliente">Agua Caliente</option>
                      <option value="vip">VIP</option>
                  </select>
              </div>
              <div class="form-group">
                  <input runat="server" style="display:none;" type="text" name="name" class="form-control" id="mostrarInputMuseo1" placeholder="Horario Entrada" data-rule="minlen:2" data-msg="Please enter at least 2 chars" />
                  <div class="validation"></div>
              </div>
              <div class="form-group">
                  <input runat="server" style="display:none;" type="text" name="name" class="form-control" id="mostrarInputMuseo2" placeholder="Horario Salida" data-rule="minlen:2" data-msg="Please enter at least 2 chars" />
                  <div class="validation"></div>
              </div>
              <div class="form-group">
                  <input  runat="server" style="display:none;" type="number" name="name" class="form-control" id="mostrarInputMuseo3" placeholder="Tarifa Unica" data-rule="minlen:2" data-msg="Please enter at least 2 chars" />
                  <div class="validation"></div>
              </div>               
              <div class="text-center"><button type="submit" runat="server" id ="btnRegistrarEmpresa" onserverclick="btnRegistrarEmpresa_Click">Enviar Solicitud</button></div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- #contact -->


  <!--==========================
    Footer
  ============================-->
  <footer class="footer">
    <div class="container">
      <div class="row">

        <div class="col-md-12 col-lg-4">
          <div class="footer-logo">

            <a class="navbar-brand" href="#">eStartup</a>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the.</p>

          </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-2">
          <div class="list-menu">

            <h4>Abou Us</h4>

            <ul class="list-unstyled">
              <li><a href="#">About us</a></li>
              <li><a href="#">Features item</a></li>
              <li><a href="#">Live streaming</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>

          </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-2">
          <div class="list-menu">

            <h4>Abou Us</h4>

            <ul class="list-unstyled">
              <li><a href="#">About us</a></li>
              <li><a href="#">Features item</a></li>
              <li><a href="#">Live streaming</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>

          </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-2">
          <div class="list-menu">

            <h4>Support</h4>

            <ul class="list-unstyled">
              <li><a href="#">faq</a></li>
              <li><a href="#">Editor help</a></li>
              <li><a href="#">Contact us</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>

          </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-2">
          <div class="list-menu">

            <h4>Abou Us</h4>

            <ul class="list-unstyled">
              <li><a href="#">About us</a></li>
              <li><a href="#">Features item</a></li>
              <li><a href="#">Live streaming</a></li>
              <li><a href="#">Privacy Policy</a></li>
            </ul>

          </div>
        </div>

      </div>
    </div>

    <div class="copyrights">
      <div class="container">
        <p>&copy; Copyrights eStartup. All rights reserved.</p>
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
          -->
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
    </div>

  </footer>



  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="libInicio/jquery/jquery.min.js"></script>
  <script src="libInicio/jquery/jquery-migrate.min.js"></script>
  <script src="libInicio/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="libInicio/superfish/hoverIntent.js"></script>
  <script src="libInicio/superfish/superfish.min.js"></script>
  <script src="libInicio/easing/easing.min.js"></script>
  <script src="libInicio/modal-video/js/modal-video.js"></script>
  <script src="libInicio/owlcarousel/owl.carousel.min.js"></script>
  <script src="libInicio/wow/wow.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactformInicio/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="jsInicio/main.js"></script>

  <script>
    function activarRestaurante(){
      var contenedor = document.getElementById("mostrarInput");
      contenedor.style.display = "block";
      return true;
    }

    function activarHotel(){
      var contenedor = document.getElementById("mostrarInputHotel1");
      var contenedor2 = document.getElementById("mostrarInputHotel2");
      contenedor.style.display = "block";
      contenedor2.style.display = "block";
      return true;
    }

    function activarMuseo(){
      var contenedor = document.getElementById("mostrarInputMuseo1");
      var contenedor2 = document.getElementById("mostrarInputMuseo2");
      var contenedor3 = document.getElementById("mostrarInputMuseo3");
      contenedor.style.display = "block";
      contenedor2.style.display = "block";
      contenedor3.style.display = "block";
      return true;
    }

    function desactivarRestaurante(){
      var contenedor = document.getElementById("mostrarInput");
      contenedor.style.display = "none";
      return true;
    }

    function desactivarHotel(){
      var contenedor = document.getElementById("mostrarInputHotel1");
      var contenedor2 = document.getElementById("mostrarInputHotel2");
      contenedor.style.display = "none";
      contenedor2.style.display = "none";
      return true;
    }

    function desactivarMuseo(){
      var contenedor = document.getElementById("mostrarInputMuseo1");
      var contenedor2 = document.getElementById("mostrarInputMuseo2");
      var contenedor3 = document.getElementById("mostrarInputMuseo3");
      contenedor.style.display = "none";
      contenedor2.style.display = "none";
      contenedor3.style.display = "none";
      return true;
    }

    var select = document.getElementById('idEmpresa');
    select.addEventListener('change',function(){
      var selectedOption = this.options[select.selectedIndex];
      

      if(selectedOption.value == 1){
        activarRestaurante();
        desactivarHotel();
        desactivarMuseo();
      }else if(selectedOption.value == 2){
        desactivarRestaurante();
        activarHotel();
        desactivarMuseo();
      }else if(selectedOption.value == 3){
        desactivarRestaurante();
        desactivarHotel();
        activarMuseo();
      }


    });
  </script>

    </form>

</body>
</html>
