﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class adminReporteEmpresas : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			cantidadNorte();
			cantidadSur();
			cantidadEste();
			cantidadOeste();
			tipoRestaurante();
			tipoMuseo();
			tipoHotel();
			estado1();
			estado2();
			estado3();
			estado4();
		}

		private void cantidadNorte()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE region = 1";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasNorte.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void cantidadSur()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE region = 2";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasSur.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void cantidadEste()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE region = 3";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasEste.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void cantidadOeste()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE region = 4";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasOeste.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void tipoRestaurante()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE tipo = 1";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					Literal1.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void tipoHotel()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE tipo = 2";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					Literal2.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void tipoMuseo()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE tipo = 3";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					Literal3.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void estado1()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE estado = 1";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					Literal4.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void estado2()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE estado = 2";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					Literal5.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void estado3()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE estado = 3";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					Literal6.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void estado4()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM empresa WHERE estado = 4";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					Literal7.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

	}
}