﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class adminReporteSitio : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			cantidadNorte();
			cantidadSur();
			cantidadEste();
			cantidadOeste();
		}

		private void cantidadNorte()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM sitio_turistico WHERE region = 1";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasNorte.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void cantidadSur()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM sitio_turistico WHERE region = 2";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasSur.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void cantidadEste()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM sitio_turistico WHERE region = 3";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasEste.Text = dr.GetString(0);
				}
			}
			con.Close();
		}

		private void cantidadOeste()
		{
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM sitio_turistico WHERE region = 4";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					numeroEmpresasOeste.Text = dr.GetString(0);
				}
			}
			con.Close();
		}
	}
}