﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class adminEliminar : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;

		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT u.dpi,u.nombre,u.correo,u.telefono,u.usuario,u.contrasenia,r.rol FROM usuario u, rol r WHERE u.rol = r.codigo;";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					escribirHTML.Text += "" +
						"<tr class=\"table-dark\">" +
						"	<td scope='row'>" + dr.GetString(0) + "</td>" +
						"	<td>" + dr.GetString(1) + "</td>" +
						"	<td>" + dr.GetString(2) + "</td>" +
						"	<td>" + dr.GetString(3) + "</td>" +
						"	<td>" + dr.GetString(4) + "</td>" +
						"	<td>" + dr.GetString(5) + "</td>" +
						"	<td>" + dr.GetString(6) + "</td>" +
						"<td><button type=\"button\" class=\"btn btn-danger btn-delete\" data-toggle=\"modal\" data-target=\"#exampleModal\">Eliminar</button></td>" +
						"</tr>";// +
						//"<script>document.getElementById(\"btn1\").addEventListener(\"click\", function(document.getElementById(\"pr\").innerHTML = \"Hello World\";});</ script > ";
				}
			}
			con.Close();

		}

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			string id = idEliminar.Value;

			if (id != "1") {
				connectionString();
				con.Open();
				com.Connection = con;
				com.CommandText = "DELETE FROM usuario WHERE dpi = " + id + ";";
				com.ExecuteNonQuery();
				con.Close();
				Response.Redirect("adminEliminar.aspx");
			} else {
				Response.Redirect("adminEliminar.aspx");
			}

			
		}
	}
}