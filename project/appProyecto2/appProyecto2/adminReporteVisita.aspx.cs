﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class adminReporteVisita : System.Web.UI.Page
	{
		MySqlConnection con = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlDataReader dr;
		List<String> nombres = new List<String>();

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
			tecnicos();

			for (int i = 0; i < (nombres.Count()); i++) {
				Literal1.Text += "<p class=\"card-text\">" + nombres.ElementAt(i) + " tiene " + conteo(nombres.ElementAt(i)) + " Visitas Asignadas</span>";
			}
		}

		private String conteo(String nombre)
		{
			String cont = "";
			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT COUNT(*) FROM visitaTecnica v, usuario u WHERE v.dpiTecnico = u.dpi AND u.nombre = '" + nombre + "';";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					cont = dr.GetString(0);
				}
			}
			con.Close();
			return cont;
		}


		private void tecnicos()
		{

			con.Open();
			com.Connection = con;
			com.CommandText = "SELECT nombre FROM usuario WHERE rol = 2;";
			dr = com.ExecuteReader();

			if (dr.HasRows) {
				while (dr.Read()) {
					nombres.Add(dr.GetString(0));
				}
			}
			con.Close();
		}

	}
}