﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agenteListarSitiosTuristicos.aspx.cs" Inherits="appProyecto2.agenteListarSitiosTuristicos" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agente Turistico</title>
<!--===============================================================================================-->	
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <style>
        body {
            background: url("/images/bgAgente.jpg");
            background-size: cover;
            background-attachment:fixed;
        }

        .colorFondo {
            background-color: rgba(0,0,0,0.7);
        }

        .dd {
            padding:25px;
            background: linear-gradient(60deg,rgba(10,50,100,0.7),rgba(0,0,0,0.5));
            margin-top: 50px;
            margin-bottom: 100px;
            color: #fff;
        }

        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        #img-upload{
            width: 100%;
        }

        .carousel-caption {
            background: rgba(0,0,0,0.7);
        }

        .des {
            color:white;
        }

    </style>
</head>
<body>
  
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <a class="navbar-brand" href="agenteInicio.aspx">
                <h5>Instituto N.T <span class="badge badge-primary">Agente</span></h5>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Sitios Turisticos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteNuevoSitioTuristico.aspx">Nuevo Sitio Turistico</a>
                            <a class="dropdown-item" href="agenteListarSitiosTuristicos.aspx">Listar Sitios Turisticos</a>
                            <a class="dropdown-item" href="agenteEliminarSitioTuristico.aspx">Eliminar Sitio Turistico</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Empresas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteVerSolicitudes.aspx">Ver Solicitudes</a>
                            <a class="dropdown-item" href="agenteVerificarDatos.aspx">Verificar Datos de Empresa</a>
                            <a class="dropdown-item" href="agenteAsignarVisitaTecnica.aspx">Asignar Visita Tecnica</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Reportes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteReporteSitiosTuristicosInscritos.aspx">Reporte de Sitios Turisticos </a>
                            <a class="dropdown-item" href="agenteReporteSolicitudesEmpresas.aspx">Reporte de Empresas</a>
                            <a class="dropdown-item" href="agenteReporteVisitasTecnicasAsignadas.aspx">Reporte de Visitas Tecnicas Asignadas</a>
                            <a class="dropdown-item" href="agenteReporteEmpresasTipoHotel.aspx">Reporte de servicios de Hotel</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cuenta
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.aspx">Cerrar Sesion</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav> 


        <div class="container col-xl-10 mx-auto row">
            <!--CARROUSEL DE LOS SITIOS TURISTICOS DEL NORTE-->
                <div id="carouselExampleIndicators" class="carousel slide col-xl-4 mx-auto m-5" data-ride="carousel">
            <h3 class="text-center">Norte</h3>             
            <div class="carousel-inner">
                <asp:Literal ID="carrouselNorte" runat="server"></asp:Literal>
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/work-2.jpg" alt="First slide">
                    <div class="carousel-caption">
                        <h3>Prueba</h3>
                        <p class="des">---------------</p>
                    </div>
                </div>                              
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
       </div>

            <!--CARROUSEL DE LOS SITIOS TURISTICOS DEL SUR-->
            <div id="carouselExampleIndicators2" class="carousel slide col-xl-4 mx-auto m-5" data-ride="carousel">
            <h3 class="text-center">Sur</h3>
            <div class="carousel-inner">
                <asp:Literal ID="CarrouselSur" runat="server"></asp:Literal>
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/work-2.jpg" alt="First slide">
                    <div class="carousel-caption">
                        <h3>Prueba</h3>
                        <p class="des">---------------</p>
                    </div>
                </div>                          
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
       </div>

       </div>

        <!--Tercer y Cuarto Carrousel-->
        <div class="container col-xl-10 mx-auto row">
                <!--CARROUSEL DE LOS SITIOS TURISTICOS DEL ESTE-->
                <div id="carouselExampleIndicators3" class="carousel slide col-xl-4 mx-auto m-5" data-ride="carousel">
            <h3 class="text-center">Este</h3>
            <div class="carousel-inner">
                <asp:Literal ID="CarrouselEste" runat="server"></asp:Literal>
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/work-2.jpg" alt="First slide">
                    <div class="carousel-caption">
                        <h3>Prueba</h3>
                        <p class="des">---------------</p>
                    </div>
                </div>                                
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
       </div>

            <!--CARROUSEL DE LOS SITIOS TURISTICOS DEL OESTE-->
            <div id="carouselExampleIndicators4" class="carousel slide col-xl-4 mx-auto m-5" data-ride="carousel">
            <h3 class="text-center">Oeste</h3>
            <div class="carousel-inner">
                <asp:Literal ID="CarrouselOeste" runat="server"></asp:Literal>
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/work-2.jpg" alt="First slide">
                    <div class="carousel-caption">
                        <h3>Prueba</h3>
                        <p class="des">---------------</p>
                    </div>
                </div>                          
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators4" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators4" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
       </div>

       </div>

    <!--Fin del Div Principal-->
    </div>




        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>

</body>
</html>