﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agenteReporteVisitasTecnicasAsignadas.aspx.cs" Inherits="appProyecto2.agenteReporteVisitasTecnicasAsignadas" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agente Turistico</title>
<!--===============================================================================================-->	
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <style>
        body {
            background: url("/images/bgAgente.jpg");
            background-size: cover;
            background-attachment:fixed;
        }

        .dd {
            margin-top:40px;
        }
    </style>
</head>
<body>
  
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <a class="navbar-brand" href="agenteInicio.aspx">
                <h5>Instituto N.T <span class="badge badge-primary">Agente</span></h5>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Sitios Turisticos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteNuevoSitioTuristico.aspx">Nuevo Sitio Turistico</a>
                            <a class="dropdown-item" href="agenteListarSitiosTuristicos.aspx">Listar Sitios Turisticos</a>
                            <a class="dropdown-item" href="agenteEliminarSitioTuristico.aspx">Eliminar Sitio Turistico</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Modulo Empresas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteVerSolicitudes.aspx">Ver Solicitudes</a>
                            <a class="dropdown-item" href="agenteVerificarDatos.aspx">Verificar Datos de Empresa</a>
                            <a class="dropdown-item" href="agenteAsignarVisitaTecnica.aspx">Asignar Visita Tecnica</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Reportes
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="agenteReporteSitiosTuristicosInscritos.aspx">Reporte de Sitios Turisticos </a>
                            <a class="dropdown-item" href="agenteReporteSolicitudesEmpresas.aspx">Reporte de Empresas</a>
                            <a class="dropdown-item" href="agenteReporteVisitasTecnicasAsignadas.aspx">Reporte de Visitas Tecnicas Asignadas</a>
                            <a class="dropdown-item" href="agenteReporteEmpresasTipoHotel.aspx">Reporte de servicios de Hotel</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cuenta
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.aspx">Cerrar Sesion</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav> 

        <div class="container col-xl-12 mx-auto row m-4 d-flex justify-content-center">
            
            <div class="card mb-3 col-xl-4 p-2 m-4">
                <img class="card-img-top" src="imgInicio/screen/t1.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Tecnicos</h5>                   
                    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                    <p class="card-text"><small class="text-muted">Reporte de Tecnicos Asignados a Visitas</small></p>
                </div>
            </div>           
        </div>
        
    <!--FIN DEL CONTENEDOR--> 
    </div>




        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>
    
</body>
</html>
