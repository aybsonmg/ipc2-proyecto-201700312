﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace appProyecto2
{
	public partial class inicio : System.Web.UI.Page
	{

		MySqlConnection con = new MySqlConnection();
		MySqlConnection conInsert = new MySqlConnection();
		MySqlCommand com = new MySqlCommand();
		MySqlCommand comInsert = new MySqlCommand();
		MySqlDataReader dr;

		void connectionString()
		{
			con.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
			conInsert.ConnectionString = "Server = localhost; UserID = root; Database = inat; Password = realmadrid12";
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			connectionString();
		}

		protected void btnRegistrar_Click(object sender, EventArgs e) {
			String nombre = nombreUsuario.Value;
			String correo = emailUsuario.Value;
			String usuario = idUsr.Value;
			String contrasenia = idPass.Value;

			connectionString();
			con.Open();
			conInsert.Open();
			com.Connection = con;
			comInsert.Connection = conInsert;

			if (nombre == "" || correo == "" || usuario == "" || contrasenia == "" ) {
				Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
			} else {
				com.CommandText = "SELECT COUNT(*) FROM visitantes WHERE usuario='" + usuario + "';";
				dr = com.ExecuteReader();

				if (dr.HasRows) {
					while (dr.Read()) {
						if (dr.GetString(0) == "0") {
							comInsert.CommandText = "INSERT INTO visitantes VALUES ('"+nombre+"','"+correo+"','"+usuario+"','"+contrasenia+"');";
							comInsert.ExecuteReader();							
							Literal1.Text = "<div class=\"alert alert-success\" role=\"alert\">Usuario Registrado!</div>";
							conInsert.Close();
						} else {
							Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\">El usuario  que esta tratando de registrar, ya existe!</div>";
							conInsert.Close();
						}
					}
				}
			}
			con.Close();
		}


		protected void btnRegistrarEmpresa_Click(object sender, EventArgs e)
		{
			
			String registro = registroEmpresa.Value;
			String nombre = nombreEmpresa.Value;
			String direccion = direccionEmpresa.Value;
			String telefono = telefonoEmpresa.Value;
			String correo = correoEmpresa.Value;
			String region = regionEmpresa.Value;
			String tipo = idEmpresa.Value;

			//Restaurante
			String especialidad = mostrarInput.Value;

			//Hotel
			String servicio1 = mostrarInputHotel1.Value;
			String servicio2 = mostrarInputHotel2.Value;

			//Museo
			String horarioA = mostrarInputMuseo1.Value;
			String horarioC = mostrarInputMuseo2.Value;
			String tarifa = mostrarInputMuseo3.Value;

			//Abrir Conexion
			connectionString();
			con.Open();
			conInsert.Open();
			com.Connection = con;
			comInsert.Connection = conInsert;

			//Verificar si existe la empresa

			if (registro == "" || nombre == "" || direccion == "" || telefono == "" || correo == "" || region == "0" || tipo == "0" ) {
				Literal2.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
			} else {
				com.CommandText = "SELECT COUNT(*) FROM empresa WHERE codigo=" + registro + ";";
				dr = com.ExecuteReader();

				if (dr.HasRows) {
					while (dr.Read()) {
						if (dr.GetString(0) == "0") {


							if (tipo == "1" ) {
								if (especialidad == "") {
									Literal2.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
								} else {
									comInsert.CommandText = "INSERT INTO empresa VALUES (" + registro + ",'" + nombre + "','" + direccion + "'," + telefono + ",'" + correo + "',null," + region + ",1," + tipo + ");" +
									" INSERT INTO especialidad VALUES (" + registro + ",'" + especialidad + "');";
									comInsert.ExecuteReader();
									Literal2.Text = "<div class=\"alert alert-success\" role=\"alert\">Usuario Registrado!</div>";
								}								
							}

							if (tipo == "2") {
								if (servicio1 == "" || servicio2 == "") {
									Literal2.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
								} else {
									comInsert.CommandText = "INSERT INTO empresa VALUES (" + registro + ",'" + nombre + "','" + direccion + "'," + telefono + ",'" + correo + "',null," + region + ",1," + tipo + ");" +
									" INSERT INTO servicio(empresa,servicio) VALUES (" + registro + ",'" + servicio1 + "');" +
									"INSERT INTO servicio(empresa,servicio) VALUES (" + registro + ",'" + servicio2 + "');";
									comInsert.ExecuteReader();
									Literal2.Text = "<div class=\"alert alert-success\" role=\"alert\">Usuario Registrado!</div>";
								}
							}

							if (tipo == "3") {
								if (horarioA == "" || horarioC == "" || tarifa == "") {
									Literal2.Text = "<div class=\"alert alert-danger\" role=\"alert\">Debe de Completar todos los campos(*)!</div>";
								} else {
									comInsert.CommandText = "INSERT INTO empresa VALUES (" + registro + ",'" + nombre + "','" + direccion + "'," + telefono + ",'" + correo + "',null," + region + ",1," + tipo + ");" +
									" INSERT INTO detalle_museo VALUES (" + registro + ",'" + horarioA + "','"+horarioC+"',"+tarifa+");";
									comInsert.ExecuteReader();
									Literal2.Text = "<div class=\"alert alert-success\" role=\"alert\">Usuario Registrado!</div>";
								}
							}
						
							conInsert.Close();
						} else {
							Literal2.Text = "<div class=\"alert alert-danger\" role=\"alert\">El usuario  que esta tratando de registrar, ya existe!</div>";
							conInsert.Close();
						}
					}
				}			
			}

			//Cierre Conexion
			con.Close();

		}

	}
}